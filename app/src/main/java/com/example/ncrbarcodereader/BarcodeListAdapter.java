package com.example.ncrbarcodereader;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class BarcodeListAdapter  extends RecyclerView.Adapter<BarcodeListAdapter.BarcodeListViewHolder>{

    private List<BarcodeStorageInformationClass> dataList;
   public BarcodeListAdapter(List<BarcodeStorageInformationClass> list){
       this.dataList =list;
   }
    @NonNull
    @Override
    public BarcodeListViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        BarcodeListViewHolder holder = new BarcodeListViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.barcode_listitems_layout, viewGroup, false));
        return holder;

    }

    @Override
    public void onBindViewHolder(@NonNull BarcodeListViewHolder barcodeListViewHolder, int i) {
        BarcodeStorageInformationClass details = dataList.get(i);
        barcodeListViewHolder.typeTextView.setText(details.getType());
        barcodeListViewHolder.descTextView.setText(details.getUrl());
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    class BarcodeListViewHolder extends RecyclerView.ViewHolder{
        TextView typeTextView;
        TextView descTextView;

        public BarcodeListViewHolder(@NonNull View itemView) {
            super(itemView);
            typeTextView = itemView.findViewById(R.id.type_text_view);
            descTextView = itemView.findViewById(R.id.description_text_view);
        }
    }
}
