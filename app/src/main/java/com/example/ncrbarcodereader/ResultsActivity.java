package com.example.ncrbarcodereader;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class ResultsActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);
        initList();
    }
    private void initList() {
        recyclerView = findViewById(R.id.recyclerView);
        List<BarcodeStorageInformationClass> list = new ArrayList<>();
       for (int i = 1; i <= 100; i++) {
            BarcodeStorageInformationClass details = new BarcodeStorageInformationClass("Type " + i, "Description " + i);
            list.add(details);
        }

        list = this.getIntent().getParcelableArrayListExtra("BarcodeStorageInformationClass");
        BarcodeListAdapter adapter = new BarcodeListAdapter(list);
        recyclerView.setAdapter(adapter);

        LinearLayoutManager manager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(manager);
    }
}
