package com.example.ncrbarcodereader;

import android.os.Parcel;
import android.os.Parcelable;

public class BarcodeStorageInformationClass implements Parcelable {
//   public  BarcodeStorageInformationClass(String typr){
//       this.type =type;
//   }

    public BarcodeStorageInformationClass(String type, String url) {
        this.type = type;
        this.url = url;
    }

    String type;
    String url;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }



    public void setUrl(String url) {
        this.url = url;
    }
    public BarcodeStorageInformationClass(Parcel in){
        this.type = in.readString();
        this.url = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(type);
        parcel.writeString(url);

    }
    public static final Parcelable.Creator<BarcodeStorageInformationClass> CREATOR = new Parcelable.Creator<BarcodeStorageInformationClass>() {

        public BarcodeStorageInformationClass createFromParcel(Parcel in) {
            return new BarcodeStorageInformationClass(in);
        }

        public BarcodeStorageInformationClass[] newArray(int size) {
            return new BarcodeStorageInformationClass[size];
        }
    };


}
