package com.example.ncrbarcodereader;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcode;
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcodeDetector;
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcodeDetectorOptions;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
 private Button scan ,btn;
    private ProgressBar progressBar;
   private Bitmap bmp;
    static final int REQUEST_IMAGE_CAPTURE = 1;
    private static final int PERMISSION_REQUEST_CODE = 200;
    private ImageView capturedImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
       scan = (Button) findViewById(R.id.scan);
       btn = (Button) findViewById(R.id.results);
       btn.setEnabled(false);
        progressBar = (ProgressBar) findViewById(R.id.progressBar_cyclic);


    }

    public void openCamera(View view) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {

            Bundle extras = data.getExtras();
            bmp = (Bitmap) extras.get("data");


            //  bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
            capturedImage = (ImageView) findViewById(R.id.capturedImage);

            capturedImage.setImageBitmap(bmp);
            btn.setEnabled(true);
        }

    }
    public void popReults( View view) {
        progressBar.setVisibility(view.VISIBLE);

        //  Configure the barcode detector
        FirebaseVisionBarcodeDetectorOptions options =
                new FirebaseVisionBarcodeDetectorOptions.Builder()
                        .setBarcodeFormats(
                                FirebaseVisionBarcode.FORMAT_QR_CODE,
                                FirebaseVisionBarcode.FORMAT_AZTEC)
                        .build();

        // FirebaseVisionImage image = FirebaseVisionImage.fromBitmap(bitmap);VisionImage.java
        FirebaseVisionImage image = FirebaseVisionImage.fromBitmap(bmp);


        // [START get_detector]
        FirebaseVisionBarcodeDetector detector = FirebaseVision.getInstance()
                .getVisionBarcodeDetector();




        Task<List<FirebaseVisionBarcode>> result = detector.detectInImage(image)
                .addOnSuccessListener(new OnSuccessListener<List<FirebaseVisionBarcode>>() {


                    @Override
                    public void onSuccess(List<FirebaseVisionBarcode> barcodes) {


                        // Task completed successfully

                        ArrayList<BarcodeStorageInformationClass> barcodesList = new ArrayList<>();

                        // ...
                        for (FirebaseVisionBarcode barcode : barcodes) {
                            Rect bounds = barcode.getBoundingBox();
                            Point[] corners = barcode.getCornerPoints();

                            String rawValue = barcode.getRawValue();
                           // Toast.makeText(MainActivity.this,rawValue,Toast.LENGTH_SHORT).show();

                            int valueType = barcode.getValueType();
                            // See API reference for complete list of supported types

                            //Toast.makeText(MainActivity.this,valueType,Toast.LENGTH_SHORT).show();

                            switch (valueType) {


                                case FirebaseVisionBarcode.TYPE_WIFI:
                                    String ssid = barcode.getWifi().getSsid();
                                    String password = barcode.getWifi().getPassword();

                                    int type = barcode.getWifi().getEncryptionType();
                                    String value = "SSID :" + ssid + ", " + "Password: " + password;
                                    String details = "Type: " + "WIFI" + "\n" + "SSID: " + ssid + "\n" + "PASSWORD: " + password + "\n" + "ENCRYPTION: " + (type);
                                    BarcodeStorageInformationClass b1 = new BarcodeStorageInformationClass(value,details);
                                    barcodesList.add(b1);
                                    break;
                                case FirebaseVisionBarcode.TYPE_URL:
                                    String title1;
                                    title1 = barcode.getUrl().getTitle();
                                    String url1 = barcode.getUrl().getUrl();

                                    value = "URL: " + rawValue;
                                    details = "Type: " + "URL" + "\n" + "<b>URL: <b> " + rawValue + "\n" + "TITLE: " + title1;

                                    BarcodeStorageInformationClass b2 =new BarcodeStorageInformationClass(value,details);
                                    barcodesList.add(b2);

                                    break;
                                case FirebaseVisionBarcode.TYPE_PHONE:
                                    String number = barcode.getPhone().getNumber();
                                    int types = barcode.getPhone().getType();

                                    value = "Phone Number: " + number;
                                    details = "Type: " + "PHONE" + "\n" + "PHONE-TYPE:  " + types + "\n" + "Number: " + number;
                                    BarcodeStorageInformationClass b3 =new BarcodeStorageInformationClass(value,details);
                                    barcodesList.add(b3);
                                    break;
                                case FirebaseVisionBarcode.TYPE_SMS:
                                    String message = barcode.getSms().getMessage();
                                    String phonenumber = barcode.getSms().getPhoneNumber();

                                    value = "Message: " + message;
                                    details = "Type: " + "SMS" + "\n" + "Number:  " + phonenumber + "\n" + "Message: " + message;
                                    BarcodeStorageInformationClass b4 =new BarcodeStorageInformationClass(value,details);
                                    barcodesList.add(b4);
                                    break;

                                case FirebaseVisionBarcode.TYPE_CALENDAR_EVENT:
                                    String titleCalender = barcode.getCalendarEvent().getDescription();
                                    String urlCalender = barcode.getCalendarEvent().getSummary();
                                    BarcodeStorageInformationClass b5 =new BarcodeStorageInformationClass(titleCalender,urlCalender);
                                    barcodesList.add(b5);

                                    break;
                                case FirebaseVisionBarcode.TYPE_EMAIL:
                                    String address = barcode.getEmail().getAddress();
                                    String body = barcode.getEmail().getBody();
                                    String subject = barcode.getEmail().getSubject();

                                    value = "Message: " + body;
                                    details = "Type: " + "EMAIL" + "\n" + "From Email:  " + address + "\n" + "Subject: " + subject + "\n" + "Body: " + body;
                                    BarcodeStorageInformationClass b6 =new BarcodeStorageInformationClass(value,details);
                                    barcodesList.add(b6);
                                    break;
                                case FirebaseVisionBarcode.TYPE_TEXT:
                                    value = "Text: " + rawValue;
                                    details = "Type: " + "TEXT" + "\n" + "Data:  " + rawValue;
                                    BarcodeStorageInformationClass b7 =new BarcodeStorageInformationClass(value,details);
                                    barcodesList.add(b7);
                                    break;
                                case FirebaseVisionBarcode.TYPE_GEO:
                                    double lat = barcode.getGeoPoint().getLat();
                                    double lon = barcode.getGeoPoint().getLng();

                                    value = "Lat: " + String.valueOf(lat) + " long: " + String.valueOf(lon);
                                    details = "Type: " + "GEO" + "\n" + "Lattitude:  " + (lat) + "\n" + "bLongitude: b" + lon;
                                    BarcodeStorageInformationClass b8 =new BarcodeStorageInformationClass(value,details);
                                    barcodesList.add(b8);
                                    break;

                                //  Toast.makeText(getApplicationContext(),Toast.LENGTH_SHORT).show();
                            }


                        }
                        Intent intent = new Intent(MainActivity.this, ResultsActivity.class);
                        Bundle bundle = new Bundle();
                       // bundle.putParcelableArrayList("BarcodeStorageInformationClass", barcodesList);
                       intent.putExtra("BarcodeStorageInformationClass",barcodesList);

                        startActivity(intent);

                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        // Task failed with an exception
                        // ...
                        Toast.makeText(MainActivity.this,"Failed Barcode Scan",Toast.LENGTH_SHORT).show();
                    }
                });
    }
}






